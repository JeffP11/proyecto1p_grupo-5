/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;

/**
 * Clase que almacena la información de cada programa con su constructor,
 * getters y setters Contiene el método toString()
 *
 * @author Jeffrey Prado
 */
public class Programa {

    private int codPrograma;
    private String nombrePro;
    private String descripcionPro;
    private String diasPro;
    private LocalTime horaInicio;
    private LocalTime horaFin;
    private ArrayList<Locutor> locutores;
    private Date fechaCreacion;
    private Date fechafinal;

    public Programa(int codPrograma, String nombrePro, String descripcionPro, String diasPro, LocalTime horaInicio, LocalTime horaFin, ArrayList<Locutor> locutores, Date fechaCreacion, Date fechafinal) {
        this.codPrograma = codPrograma;
        this.nombrePro = nombrePro;
        this.descripcionPro = descripcionPro;
        this.diasPro = diasPro;
        this.horaInicio = horaInicio;
        this.horaFin = horaFin;
        this.locutores = locutores;
        this.fechaCreacion = fechaCreacion;
        this.fechafinal = fechafinal;
    }

    public int getCodPrograma() {
        return codPrograma;
    }

    public void setCodPrograma(int codPrograma) {
        this.codPrograma = codPrograma;
    }

    public String getNombrePro() {
        return nombrePro;
    }

    public void setNombrePro(String nombrePro) {
        this.nombrePro = nombrePro;
    }

    public String getDescripcionPro() {
        return descripcionPro;
    }

    public void setDescripcionPro(String descripcionPro) {
        this.descripcionPro = descripcionPro;
    }

    public String getDiasPro() {
        return diasPro;
    }

    public void setDiasPro(String diasPro) {
        this.diasPro = diasPro;
    }

    public LocalTime getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(LocalTime horaInicio) {
        this.horaInicio = horaInicio;
    }

    public LocalTime getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(LocalTime horaFin) {
        this.horaFin = horaFin;
    }

    public ArrayList<Locutor> getLocutores() {
        return locutores;
    }

    public void setLocutores(ArrayList<Locutor> locutores) {
        this.locutores = locutores;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechafinal() {
        return fechafinal;
    }

    public void setFechafinal(Date fechafinal) {
        this.fechafinal = fechafinal;
    }

    @Override
    public String toString() {
        return "Programa{" + "codPrograma=" + codPrograma + ", nombrePro=" + nombrePro + ", descripcionPro=" + descripcionPro + ", diasPro=" + diasPro + ", horaInicio=" + horaInicio + ", horaFin=" + horaFin + ", locutores=" + locutores + ", fechaCreacion=" + fechaCreacion + ", fechafinal=" + fechafinal + '}';
    }
}
