/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;
import java.util.Date;

/**
 * Clase que almacena la informacion de cada concurso con su constructor,
 * getters y setters Contiene el método toString()
 *
 * @author Jeffrey Prado
 */
public class Concurso {

    private int codConcurso;
    private String nombreCon;
    private String estado;
    private Date fechaInicioCon;
    private Date fechaFinCon;
    private Programa programa;
    private String premio;
    private ArrayList<Persona> participantes;
    private Persona ganador;

    public Concurso(int codConcurso, String nombreCon, String estado, Date fechaInicioCon, Date fechaFinCon, Programa programa, String premio, ArrayList<Persona> participantes, Persona ganador) {
        this.codConcurso = codConcurso;
        this.nombreCon = nombreCon;
        this.estado = estado;
        this.fechaInicioCon = fechaInicioCon;
        this.fechaFinCon = fechaFinCon;
        this.programa = programa;
        this.premio = premio;
        this.participantes = participantes;
        this.ganador = ganador;
    }

    public int getCodConcurso() {
        return codConcurso;
    }

    public void setCodConcurso(int codConcurso) {
        this.codConcurso = codConcurso;
    }

    public String getNombreCon() {
        return nombreCon;
    }

    public void setNombreCon(String nombreCon) {
        this.nombreCon = nombreCon;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFechaInicioCon() {
        return fechaInicioCon;
    }

    public void setFechaInicioCon(Date fechaInicioCon) {
        this.fechaInicioCon = fechaInicioCon;
    }

    public Date getFechaFinCon() {
        return fechaFinCon;
    }

    public void setFechaFinCon(Date fechaFinCon) {
        this.fechaFinCon = fechaFinCon;
    }

    public Programa getPrograma() {
        return programa;
    }

    public void setPrograma(Programa programa) {
        this.programa = programa;
    }

    public String getPremio() {
        return premio;
    }

    public void setPremio(String premio) {
        this.premio = premio;
    }

    public ArrayList<Persona> getParticipantes() {
        return participantes;
    }

    public void setParticipantes(ArrayList<Persona> participantes) {
        this.participantes = participantes;
    }

    public Persona getGanador() {
        return ganador;
    }

    public void setGanador(Persona ganador) {
        this.ganador = ganador;
    }

    @Override
    public String toString() {
        return "Concurso{" + "codConcurso=" + codConcurso + ", nombreCon=" + nombreCon + ", estado=" + estado + ", fechaInicioCon=" + fechaInicioCon + ", fechaFinCon=" + fechaFinCon + ", programa=" + programa + ", premio=" + premio + ", participantes=" + participantes + ", ganador=" + ganador + '}';
    }

}
