/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 * Clase que almacena la informacion de cada canción con su constructor, getters
 * y setters Contiene el método toString()
 *
 * @author Jeffrey Prado
 */
public class Cancion {

    public String nomArtista;
    public String nomCancion;
    public int ubicacionTop;

    public Cancion(String nomArtista, String nomCancion, int ubicacionTop) {
        this.nomArtista = nomArtista;
        this.nomCancion = nomCancion;
        this.ubicacionTop = ubicacionTop;
    }

    public String getNomArtista() {
        return nomArtista;
    }

    public void setNomArtista(String nomArtista) {
        this.nomArtista = nomArtista;
    }

    public String getNomCancion() {
        return nomCancion;
    }

    public void setNomCancion(String nomCancion) {
        this.nomCancion = nomCancion;
    }

    public int getUbicacionTop() {
        return ubicacionTop;
    }

    public void setUbicacionTop(int ubicacionTop) {
        this.ubicacionTop = ubicacionTop;
    }

    @Override
    public String toString() {
        return "Cancion{" + "nomArtista=" + nomArtista + ", nomCancion=" + nomCancion + ", ubicacionTop=" + ubicacionTop + '}';
    }

}
