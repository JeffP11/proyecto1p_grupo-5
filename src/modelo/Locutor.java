/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;

/**
 * Clase que almacena la información de cada locutor incluyendo sus redes
 * sociales con su constructor, getters y setters
 *
 * @author Sebastian Benalcazar
 */
public class Locutor extends Persona {

    private ArrayList<Redes> redSociales;

    public Locutor(ArrayList<Redes> redSociales, String cedula, String nombre, String apellidos, String telefono) {
        super(cedula, nombre, apellidos, telefono);
        this.redSociales = redSociales;
    }

    public ArrayList<Redes> getRedSociales() {
        return redSociales;
    }

    public void setRedSociales(ArrayList<Redes> redSociales) {
        this.redSociales = redSociales;
    }

}
