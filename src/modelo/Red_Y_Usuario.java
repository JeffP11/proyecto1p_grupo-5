/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 * Clase que guarda las redes sociales para cada persona
 *
 * @author Alexis Poveda
 */
public class Red_Y_Usuario {

    private Redes redSocial;
    private Persona usuario;

    public Redes getRedSocial() {
        return redSocial;
    }

    public void setRedSocial(Redes redSocial) {
        this.redSocial = redSocial;
    }

    public Persona getUsuario() {
        return usuario;
    }

    public void setUsuario(Persona usuario) {
        this.usuario = usuario;
    }

    public Red_Y_Usuario(Redes redSocial, Persona usuario) {
        this.redSocial = redSocial;
        this.usuario = usuario;
    }
}
