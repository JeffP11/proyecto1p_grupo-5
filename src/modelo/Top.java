/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

/**
 * Clase que almacena los top 5 con su fecha y el respectivo arreglo de las
 * canciones
 *
 * @author Jeffrey Prado
 */
public class Top {

    private Date fecha;
    private ArrayList<Cancion> cancionesTopFecha;

    public Top(Date fecha, ArrayList<Cancion> cancionesTopFecha) {
        this.fecha = fecha;
        this.cancionesTopFecha = cancionesTopFecha;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public ArrayList<Cancion> getCancionesTopFecha() {
        return cancionesTopFecha;
    }

    public void setCancionesTopFecha(ArrayList<Cancion> cancionesTopFecha) {
        this.cancionesTopFecha = cancionesTopFecha;
    }

    @Override
    public String toString() {
        return "Top{" + "fecha=" + fecha + ", cancionesTopFecha=" + cancionesTopFecha + '}';
    }

}

