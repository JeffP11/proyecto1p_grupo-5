/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto1_grupo5;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Random;
import java.util.Scanner;
import modelo.Cancion;
import modelo.Concurso;
import modelo.Locutor;
import modelo.Programa;
import modelo.Persona;
import modelo.Red_Y_Usuario;
import modelo.Redes;
import modelo.Top;

/**
 * @author Jeffrey Prado
 * @author Alexis Poveda
 * @author Sebastian Benalcazar
 */
public class Radio {

    public String nombreRadio;
    public String descripcion;
    public String dial;
    public String direccion;
    public String telefono;
    public Redes redesSociales;

    private ArrayList<Persona> listParticipantes = new ArrayList<>();
    private ArrayList<Locutor> listLocutores = new ArrayList<>();
    private ArrayList<Programa> listPrograma = new ArrayList<>();
    private ArrayList<Concurso> listConcurso = new ArrayList<>();
    private ArrayList<Cancion> listCanciones = new ArrayList<>();
    private ArrayList<Top> listTop = new ArrayList<>();
    private ArrayList<Red_Y_Usuario> listaRedUsuario = new ArrayList<>();

    Scanner leer = new Scanner(System.in);
    int contProgramas = 3;

    public static void main(String[] args) {
        Radio rad = new Radio();
        rad.menu();
    }

    /**
     *
     * Metodo que inicializa el ArrayList de personas
     *
     * @author Jeffrey Prado
     * @author Alexis Poveda
     * @author Sebastian Benalcazar
     */
    public void inicializarPersonas() {
        listParticipantes.add(new Persona("0952796159", "Sebastián Elías", "Benalcázar García", "0998446551"));
        listParticipantes.add(new Persona("0953395373", "Jeffrey Gabriel", "Prado Reyes", "0997364824"));
        listParticipantes.add(new Persona("0952796155", "Miguel Angel", "Mantilla Mera", "0998446555"));
        listParticipantes.add(new Persona("0952796158", "Matías Alfredo", "Benalcázar García", "0998446552"));
        listParticipantes.add(new Persona("0952796157", "Nicole Stefanie", "Benalcázar García", "0998446553"));
        listParticipantes.add(new Persona("0952796156", "Harry Emilio", "García Villacis", "0998446554"));
        listParticipantes.add(new Persona("0951655313", "Miguel Antonio", "Vera Vera", "0924819685"));
    }

    /**
     *
     * Metodo que inicializa el ArrayList de locutores
     *
     * @author Jeffrey Prado
     * @author Alexis Poveda
     * @author Sebastian Benalcazar
     */
    public void inicializarLocutores() {
        ArrayList<Redes> redSocial;
        redSocial = new ArrayList<>();

        redSocial.add(Redes.Facebook);
        redSocial.add(Redes.Instagram);
        redSocial.add(Redes.Twitter);
        listLocutores.add(new Locutor(redSocial, "0952796159", "Sebastián Elías", "Benalcázar García", "0998446551"));
        listLocutores.add(new Locutor(redSocial, "0952796158", "Matías Alfredo", "Benalcázar García", "0998446552"));
        listLocutores.add(new Locutor(redSocial, "0952796157", "Nicole Stefanie", "Benalcázar García", "0998446553"));
        listLocutores.add(new Locutor(redSocial, "0952796156", "Harry Emilio", "García Villacis", "0998446554"));
        listLocutores.add(new Locutor(redSocial, "0952796155", "Miguel Angel", "Mantilla Mera", "0998446555"));
    }

    /**
     *
     * Metodo que inicializa el ArrayList de programas
     *
     * @author Jeffrey Prado
     * @author Alexis Poveda
     * @author Sebastian Benalcazar
     */
    public void inicializarProgramas() {
        ArrayList<Locutor> locu = new ArrayList<Locutor>();
        locu.add(listLocutores.get(0));
        locu.add(listLocutores.get(1));
        ArrayList<Locutor> locu2 = new ArrayList<Locutor>();
        locu2.add(listLocutores.get(2));
        locu2.add(listLocutores.get(3));
        ArrayList<Locutor> locu3 = new ArrayList<Locutor>();
        locu3.add(listLocutores.get(1));
        locu3.add(listLocutores.get(4));
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date fecha1 = formatter.parse("12/08/1998");
            Date fecha2 = formatter.parse("12/08/2019");
            Date fecha3 = formatter.parse("12/08/2020");
            listPrograma.add(new Programa(1, "Despierta vago", "Programa mañanero que sirve de alarma", "Lunes,Martes,Miércoles,Jueves,Vienes", LocalTime.parse("06:00", DateTimeFormatter.ofPattern("HH:mm")), LocalTime.parse("07:00", DateTimeFormatter.ofPattern("HH:mm")), locu, fecha1, null));
            listPrograma.add(new Programa(2, "Noticias X", "Programa informativo", "Lunes,Miércoles,Vienes", LocalTime.parse("08:00", DateTimeFormatter.ofPattern("HH:mm")), LocalTime.parse("09:00", DateTimeFormatter.ofPattern("HH:mm")), locu2, fecha2, null));
            listPrograma.add(new Programa(3, "Deportes plus", "Programa de deportes nacionales", "Lunes-Vienes", LocalTime.parse("10:00", DateTimeFormatter.ofPattern("HH:mm")), LocalTime.parse("11:00", DateTimeFormatter.ofPattern("HH:mm")), locu3, fecha3, null));

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * Metodo que inicializa el ArrayList de concursos
     *
     * @author Jeffrey Prado
     * @author Alexis Poveda
     * @author Sebastian Benalcazar
     */
    public void inicializarConcursos() {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date fecha1 = formatter.parse("12/06/2018");
            Date fecha2 = formatter.parse("18/11/2018");
            ArrayList<Persona> listParticipantes1 = new ArrayList<Persona>(listParticipantes.subList(0, 5));
            ArrayList<Persona> listParticipantes2 = new ArrayList<Persona>(listParticipantes.subList(1, listParticipantes.size()));
            listConcurso.add(new Concurso(1, "Salva el semestre", "Vigente", fecha1, fecha2, listPrograma.get(0), "AP", listParticipantes1, null));
            listConcurso.add(new Concurso(2, "Salva POO", "Vencido", fecha1, fecha2, listPrograma.get(0), "AP", listParticipantes2, null));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * Metodo que inicializa el ArrayList de canciones
     *
     * @author Jeffrey Prado
     * @author Alexis Poveda
     * @author Sebastian Benalcazar
     */
    public void inicializarCanciones() {
        listCanciones.add(new Cancion("Ice Cube", "2Big", 50));
        listCanciones.add(new Cancion("Juan", "ola k ace", 1));
        listCanciones.add(new Cancion("Ozuna", "Taki taki", 100));
        listCanciones.add(new Cancion("Soda Stereo", "El rito", 2));
        listCanciones.add(new Cancion("Hombres G", "Temblando", 3));
        listCanciones.add(new Cancion("Daddy Yankee", "La gasolina", 4));
        listCanciones.add(new Cancion("Alexis Poveda", "Auxilio me desmayo", 5));
        listCanciones.add(new Cancion("Jeffrey Prado", "Creo que estoy reprobando", 6));
    }

    /**
     *
     * Metodo que inicializa el ArrayList de Top 5
     *
     * @author Jeffrey Prado
     * @author Alexis Poveda
     * @author Sebastian Benalcazar
     */
    public void inicializarTop5() {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date fecha1 = formatter.parse("17/11/2018");
            Date fecha2 = formatter.parse("18/11/2018");
            ArrayList<Cancion> listCanciones1 = new ArrayList<Cancion>(listCanciones.subList(0, 5));
            ArrayList<Cancion> listCanciones2 = new ArrayList<Cancion>(listCanciones.subList(3, listCanciones.size()));
            listTop.add(new Top(fecha1, listCanciones1));
            listTop.add(new Top(fecha2, listCanciones2));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * Método en donde está definico el menú principal
     *
     * @author Alexis Poveda
     * @author Sebastian Benalcazar
     * @author Jeffrey PRado
     */
    public void menu() {
        Radio rd = new Radio();
        rd.inicializarPersonas();
        rd.inicializarLocutores();
        rd.inicializarProgramas();
        rd.inicializarConcursos();
        rd.inicializarCanciones();
        rd.inicializarTop5();
        String opciones = "\tMANEJO DE INFORMACION\n"
                + "1.- Administrar locutores\n"
                + "2.- Administrar programas\n"
                + "3.- Administrar concursos\n"
                + "4.- Administrar Top 5\n"
                + "5.- Salir";
        int opcion = 0;
        do {
            System.out.println(opciones);
            System.out.println("Ingrese opcion: ");
            opcion = leer.nextInt();
            if (opcion > 5) {
                System.out.println("Opcion no disponible");
                System.out.println("");
            } else {
                switch (opcion) {
                    case 1:
                        rd.AdminLocutores();
                        break;
                    case 2:
                        rd.mostrarProgramas();
                        rd.admiProgramas();
                        break;
                    case 3:

                        rd.admiConcursos();
                        break;
                    case 4:
                        rd.admiTop5();
                        break;
                }

            }

        } while (opcion != 5);

    }

    /**
     * Metodo que permite mostrar los programas registrados
     *
     * @author Alexis Poveda
     */
    public void mostrarProgramas() {
        System.out.println("");
        System.out.println("=========== LISTA DE PROGRAMAS ===========");
        for (Programa p : listPrograma) {
            System.out.println("Codigo:" + p.getCodPrograma() + " \tNombre: " + p.getNombrePro() + " \nLocutores: ");
            for (Locutor l : p.getLocutores()) {
                System.out.println("Locutor: " + l.getNombre() + " " + l.getApellidos());
            }
            System.out.println("");
        }
        System.out.println("==========================================");
    }

    /**
     * Metodo que permite agregar, suspender y editar cada programa (opción 2
     * del menú principal)
     *
     * @author Alexis Poveda
     */
    public void admiProgramas() {
        Radio rd = new Radio();
        int opcion = 0;
        do {
            System.out.println("");
            System.out.println("\tMENU PROGRAMA\n"
                    + "1. Agregar programa\n"
                    + "2. Suspender programa\n"
                    + "3. Editar programa\n"
                    + "4. Volver al menú principal");
            System.out.println("Ingrese opcion: ");
            opcion = leer.nextInt();
            if (opcion > 4) {
                System.out.println("Opcion no disponible");
                System.out.println("");
            } else {
                switch (opcion) {
                    case 1:
                        rd.agregarPrograma();
                        break;
                    case 2:
                        rd.suspenderPrograma();
                        break;
                    case 3:
                        rd.editarPrograma();
                        break;
                }

            }

        } while (opcion != 4);

    }

    /**
     * Metodo que permite agregar un programa nuevo por Nombre, descripcion,
     * etc...
     *
     * @author Alexis Poveda
     */
    public void agregarPrograma() {
        Radio rd = new Radio();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            ArrayList<Locutor> lol = new ArrayList<Locutor>();
            System.out.println("Ingresar nombre de programa: ");
            String nom = leer.nextLine();
            System.out.println("Ingresar descripcion: ");
            String des = leer.nextLine();
            System.out.println("Ingresar dias: ");
            String dias = leer.next();
            System.out.println("Ingresar hora de inicio: ");
            String inicio = leer.next();
            LocalTime t1 = LocalTime.parse(inicio, DateTimeFormatter.ofPattern("HH:mm"));
            System.out.println("Ingresar hora de finalizacion: ");
            String fin = leer.next();
            LocalTime t2 = LocalTime.parse(fin, DateTimeFormatter.ofPattern("HH:mm"));
            System.out.println("");
            int oplocu = 0;
            do {
                for (Locutor l : listLocutores) {
                    System.out.println(l.getCedula() + "  -  " + l.getNombre() + " " + l.getApellidos());
                }
                System.out.println("Ingrese cedula de locutor para agregar: ");
                String cedula = leer.next();
                for (int i = 0; i < listLocutores.size(); i++) {
                    if (cedula == listLocutores.get(i).getCedula()) {
                        lol.add(listLocutores.get(i));
                    }
                }
                System.out.println("Presione 0 para dejar de agregar locutores | Presione 1 para seguir agregando");
                oplocu = leer.nextInt();
            } while (oplocu != 0);
            contProgramas += 1;
            Date fe = new Date();
            Date fechainicio = formatter.parse(formatter.format(fe));
            Date fechafin = null;
            Programa pro = new Programa(contProgramas, nom, des, dias, t1, t2, lol, fechainicio, fechafin);
            listPrograma.add(pro);

        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    /**
     * Metodo que permite suspender cualquier programa ingresando su codigo
     *
     * @author Alexis Poveda
     */
    public void suspenderPrograma() {
        System.out.println("Ingrese codigo del programa a suspender: ");
        int cod = leer.nextInt();
        for (Programa p : listPrograma) {
            if (cod == p.getCodPrograma()) {
                Date fefin = new Date();
                p.setFechafinal(fefin);
                System.out.println("Nombre: " + p.getNombrePro() + " Fecha Inicio: " + p.getFechaCreacion() + " Fecha finalizacion: " + p.getFechafinal());
                listPrograma.remove(p);
            }
        }
    }

    /**
     * Metodo que permite editar un programa segun los campos que el usuario
     * desee modificar
     *
     * @author Alexis Poveda
     */
    public void editarPrograma() {
        System.out.println("Ingrese codigo del programa: ");
        int cod = leer.nextInt();
        for (Programa p : listPrograma) {
            if (cod == p.getCodPrograma()) {
                System.out.println(p.toString());
                int op = 0;
                do {
                    System.out.println("1.- Editar nombre\n"
                            + "2.- Editar descripcion\n"
                            + "3.- Editar dias del programa\n"
                            + "4.- Editar hora de inicio del programa\n"
                            + "5.- Editar hora de finalizacion del programa\n"
                            + "6.- Editar locutores");
                    System.out.println("Ingrese opcion: ");
                    op = leer.nextInt();
                    switch (op) {
                        case 1:
                            System.out.println("Ingresar nuevo nombre: ");
                            p.setNombrePro(leer.next());
                            System.out.println("Edicion exitosa!");
                            break;
                        case 2:
                            System.out.println("Ingresar nueva descripcion: ");
                            p.setDescripcionPro(leer.next());
                            System.out.println("Edicion exitosa!");
                            break;
                        case 3:
                            System.out.println("Ingresar nuevos dias: ");
                            p.setDiasPro(leer.next());
                            System.out.println("Edicion exitosa!");
                            break;
                        case 4:
                            System.out.println("Ingresar nueva hora de inicio: ");
                            p.setHoraInicio(LocalTime.parse(leer.next()));
                            System.out.println("Edicion exitosa!");
                            break;
                        case 5:
                            System.out.println("Ingresar nueva hora de finalizacion: ");
                            p.setHoraFin(LocalTime.parse(leer.next()));
                            System.out.println("Edicion exitosa!");
                            break;
                        case 6:
                            int lit = 0;
                            do {
                                System.out.println("1.- Agregar locutor\n"
                                        + "2.- Eliminar locutor\n"
                                        + "3.- Salir");
                                System.out.println("Ingrese opcion: ");
                                lit = leer.nextInt();
                                switch (lit) {
                                    case 1:
                                        int oplocu = 0;
                                        do {
                                            System.out.println("Ingrese cedula de locutor para agregar: ");
                                            String cedula = leer.next();
                                            for (Locutor l : listLocutores) {
                                                if (cedula == l.getCedula()) {
                                                    p.getLocutores().add(l);
                                                }
                                            }
                                            System.out.println("Presione 0 para dejar de agregar locutores | Presione 1 para seguir agregando");
                                            oplocu = leer.nextInt();
                                        } while (oplocu != 0);
                                        break;
                                    case 2:
                                        int oplocu2 = 0;
                                        do {
                                            System.out.println("Ingrese cedula de locutor a eliminar: ");
                                            String cedula = leer.next();
                                            for (Locutor l : listLocutores) {
                                                if (cedula == l.getCedula()) {
                                                    p.getLocutores().remove(l);
                                                }
                                            }
                                            System.out.println("Presione 0 para dejar de eliminar locutores | Presione 1 para seguir eliminando");
                                            oplocu2 = leer.nextInt();
                                        } while (oplocu2 != 0);
                                        break;
                                }
                            } while (lit != 3);
                            break;
                    }
                } while (op != 7);
            }
        }

    }

    /**
     * Método que permite administrar los locutores (opción 1 del menú
     * principal)
     *
     * @author Sebastian Benalcazar
     */
    public void AdminLocutores() {
        //Esta función presenta un sub-menú luego de presentar los datos de locutores
        Scanner sc = new Scanner(System.in);

        System.out.println("     CI     -    Nombres y Apellidos");
        //For que permite recorrer toda la lista de locutores almacenados y presentarlos.
        for (Locutor l : listLocutores) {
            System.out.println(l.getCedula() + "  -  " + l.getNombre() + " " + l.getApellidos());
        }
        System.out.println("");
        boolean wannaContinue = true;
        while (wannaContinue) {
            System.out.println(" ============================================== ");
            System.out.println(" ==========      Menú Locutores      ========== ");
            System.out.println("    1.- Mostrar Locutor");
            System.out.println("    2.- Agregar Locutor");
            System.out.println("    3.- Eliminar Locutor");
            System.out.println("    4.- Volver al Menú Principal");
            System.out.println(" ============================================== ");
            System.out.println(" Escriba el número de la opción a seleccionar: ");
            String opcion;
            boolean iscorrect = true;
            while (iscorrect) {

                opcion = sc.nextLine();
                if ("1".equals(opcion)) {
                    MostrarLocutor();
                } else {
                    if ("2".equals(opcion)) {
                        AgregarLocutor();
                    } else {
                        if ("3".equals(opcion)) {
                            EliminarLocutor();
                        } else {
                            if ("4".equals(opcion)) {
                                wannaContinue = false;
                                iscorrect = false;
                            } else {
                                System.out.println("Opción fuera del rango. Ingrese una opción válida!");
                                wannaContinue = true;
                                iscorrect = false;
                            }
                        }
                    }
                }
            }
        }
        menu();
    }

    /**
     * Método para mostrar los locutores
     *
     * @author Sebastian Benalcazar
     */
    public void MostrarLocutor() {
        Scanner sc = new Scanner(System.in);
        boolean iscorrect = true;
        String[] digitos;
        String cedula = "";

        while (iscorrect) {
            System.out.println("Ingrese la CI del locutor: ");
            cedula = sc.next();
            digitos = cedula.split("");
            int c = 0;
            for (int i = 0; i < digitos.length; i++) {
                if ("0".equals(digitos[i]) || "1".equals(digitos[i]) || "2".equals(digitos[i]) || "3".equals(digitos[i]) || "4".equals(digitos[i]) || "5".equals(digitos[i]) || "6".equals(digitos[i]) || "7".equals(digitos[i]) || "8".equals(digitos[i]) || "9".equals(digitos[i])) {
                    c++;
                }
            }
            iscorrect = cedula.length() == 10 && c == 9;
        }

        int c = 1;
        for (Locutor l : listLocutores) {

            if (l.getCedula().equals(cedula)) {
                System.out.println(" Locutor #" + c);
                System.out.println("============================================");
                System.out.println("CI: " + l.getCedula());
                System.out.println("Nombres: " + l.getNombre());
                System.out.println("Apellidos: " + l.getApellidos());
                System.out.println("Teléfono: " + l.getTelefono());
                System.out.println("Email: " + l.getTelefono());
                System.out.println("Redes Sociales: " + l.getRedSociales().toString());
                System.out.println("============================================");
            }
            c++;
        }
        System.out.println("");
        System.out.println("");
        AdminLocutores();

    }

    /**
     * Método para agregar locutores
     *
     * @author Sebastian Benalcazar
     */
    public void AgregarLocutor() {

        Scanner sc = new Scanner(System.in);
        boolean isntcorrect = true;
        String cedula = "";
        String telef = "";
        String email = "";
        String redsocial = "";
        System.out.println(" Complete los siguientes datos para agregar su locutor: ");
        System.out.println("");
        System.out.println(" Locutor #" + (listLocutores.size() + 1));
        System.out.println("============================================");
        System.out.print("CI: ");
        isntcorrect = true;
        while (isntcorrect) {

            cedula = sc.next();
            if ((cedula.length() == 10)) {
                isntcorrect = false;
            } else {
                System.out.println("Ingrese correctamente el número de cédula");
            }
        }
        System.out.println("");
        System.out.print("Nombres: ");
        String nombres = sc.next();
        System.out.println("");
        System.out.print("Apellidos: ");
        String apellidos = sc.next();
        System.out.println("");
        System.out.print("Teléfono: ");
        isntcorrect = true;
        while (isntcorrect) {

            telef = sc.next();
            if ((telef.length() > 6) && (telef.length() < 11)) {
                isntcorrect = false;
            } else {
                System.out.println("Ingrese correctamente el número de telefono convencional o de celular");
            }
        }
        System.out.println("");
        System.out.print("Email: ");
        isntcorrect = true;
        while (isntcorrect) {

            email = sc.next();
            if (email.contains("@") && email.contains(".")) {
                isntcorrect = false;
            } else {
                System.out.println("Ingrese correctamente el nombre de la redsocial: Facebook, Twitter o Instagram!");
            }
        }
        System.out.println("");
        System.out.println("Redes Sociales: ");
        isntcorrect = true;
        ArrayList<Redes> redSociales = new ArrayList<>();
        while (isntcorrect) {

            redsocial = sc.next();
            if ("Twitter".equals(redsocial)) {
                redSociales.add(Redes.Twitter);
                isntcorrect = false;
            } else {
                if ("Facebook".equals(redsocial)) {
                    redSociales.add(Redes.Facebook);
                    isntcorrect = false;
                } else {
                    if ("Instagram".equals(redsocial)) {
                        redSociales.add(Redes.Instagram);
                        isntcorrect = false;
                    } else {
                        System.out.println("Ingrese correctamente el nombre de la redsocial: Facebook, Twitter o Instagram!");
                        isntcorrect = true;
                    }
                }
            }
            if (!isntcorrect) {
                listLocutores.add(new Locutor(redSociales, cedula, nombres, apellidos, telef));
            }
            System.out.println("¿Desea agregar otra RedSocial? s/n");
            String opc = sc.nextLine();
            if ("s".equals(opc) || "S".equals(opc)) {
                isntcorrect = true;
            }
        }
        System.out.println("");
        System.out.println("============================================");

    }

    /**
     * Método para eliminar locutores
     *
     * @author Sebastian Benalcazar
     */
    public void EliminarLocutor() {

        int oplocu2;
        do {
            System.out.println("Ingrese la cédula del locutor a eliminar: ");
            boolean isntcorrect = true;
            String cedula = "";
            while (isntcorrect) {
                cedula = leer.next();
                if (cedula.length() == 10) {
                    isntcorrect = false;
                } else {
                    isntcorrect = true;
                    System.out.println("Ingrese un número de cédula correcto!");
                }
            }

            for (Locutor l : listLocutores) {
                if (cedula.equals(l.getCedula())) {
                    listLocutores.remove(l);
                }
            }
            System.out.println("Presione 0 para dejar de eliminar locutores | Presione cualquier número para seguir eliminando");
            oplocu2 = leer.nextInt();
        } while (oplocu2 != 0);
    }

    Scanner ingreso = new Scanner(System.in);

    /**
     * Método que permite administrar los concursos (opción 3 del menú
     * principal)
     *
     * @author Jeffrey Prado
     */
    public void admiConcursos() {
        int cont = 0;
        int idConc;

        System.out.println("Código Nombre" + "  -  " + "Programa" + "  -  " + "Estado");

        for (Concurso c : listConcurso) {
            System.out.println(c.getCodConcurso() + "  -  " + c.getNombreCon() + "  -  " + c.getEstado());
        }

        Scanner subM = new Scanner(System.in);
        String subMenu = "\n"
                + "1.- Mostrar concurso\n"
                + "2.- Agregar concurso\n"
                + "3.- Eliminar concurso\n"
                + "4.- Inscribir participantes en concurso\n"
                + "5.- Asignar Ganador de concurso\n"
                + "6.- Volver al menú principal\n"
                + "Ingrese la opción que desea: ";
        boolean bandera = true;
        do {
            System.out.println(subMenu);
            String op = subM.next();
            switch (op) {
                case "1":
                    mostrarConcurso();
                    break;
                case "2":
                    agregarConcurso();
                    break;
                case "3":
                    eliminarConcurso();
                    break;
                case "4":
                    inscribirParticipantes();
                    break;
                case "5":
                    asignarGanador();
                    break;
                case "6":
                    bandera = false;
                    break;
            }
        } while (bandera);
    }

    /**
     * Método para mostrar un concurso solicitando el código del mismo
     *
     * @author Jeffrey Prado
     */
    public void mostrarConcurso() {
        System.out.println("Ingrese el código del concurso: ");
        int cod = ingreso.nextInt();
        Concurso c = listConcurso.get(cod);
        System.out.println(c.toString());
    }

    /**
     * Método para agregar un concurso con todos sus datos
     *
     * @author Jeffrey Prado
     */
    public void agregarConcurso() {
        int cont = 0;
        int idConc = ++cont;
        int condF = 1;
        do {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            try {
                System.out.println("¿Desea agregar?(S/N): ");
                String cond = ingreso.next();
                System.out.println("Ingrese el nombre del concurso: ");
                String nombre = ingreso.next();
                System.out.println("Ingrese la fecha de inicio (dd/mm/yyyy): ");
                Date fechaInicio = formatter.parse(ingreso.next());
                System.out.println("Ingrese la fecha de finalización (dd/mm/yyyy): ");
                Date fechaFin = formatter.parse(ingreso.next());
                System.out.println("Ingrese el programa en que aparece el concurso: ");
                String programaIn = ingreso.next();
                System.out.println("Ingrese el premio en efectivo: ");
                String premio = ingreso.next();
                String estado = null;
                Persona ganador = null;
                int indice = 0;
                for (Programa p : listPrograma) {
                    if (p.getNombrePro().equals(programaIn)) {
                        indice = listPrograma.indexOf(p);
                    }
                }
                Programa programaOut = listPrograma.get(listPrograma.indexOf(indice));
                Concurso c = new Concurso(idConc, nombre, estado, fechaInicio, fechaFin, programaOut, premio, listParticipantes, ganador);
                listConcurso.add(c);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        } while (condF != 0);
    }

    /**
     * Método para elminar un concurso solicitando el código del mismo
     *
     * @author Jeffrey Prado
     */
    public void eliminarConcurso() {
        System.out.println("Ingrese el código del concurso: ");
        int cod = ingreso.nextInt();
        int indice = 0;
        for (Concurso c : listConcurso) {
            if (c.getCodConcurso() == cod) {
                indice = listConcurso.indexOf(c);
                listConcurso.remove(indice);
            }
        }
    }

    /**
     * Método para inscribir participantes en un concurso
     *
     * @author Jeffrey Prado
     */
    public void inscribirParticipantes() {
        System.out.println("Código Nombre" + "  -  " + "Programa" + "  -  " + "Estado");
        for (Concurso c : listConcurso) {
            if (c.getEstado().equals("Vigente")) {
                System.out.println(c.getCodConcurso() + "  -  " + c.getNombreCon() + "  -  " + c.getEstado());
            }
        }
        System.out.println("Ingrese codigo de concurso en el que desea agregar participantes: ");
        int cod = ingreso.nextInt();
        System.out.println("Ingrese CI del participante: ");
        String cI = ingreso.next();
        System.out.println("Ingrese nombre del participante: ");
        String nomP = ingreso.next();
        System.out.println("Ingrese apellidos del participante: ");
        String apeP = ingreso.next();
        System.out.println("Ingrese telefono del participante: ");
        String telf = ingreso.next();
        Persona part = new Persona(cI, nomP, apeP, telf);
        int indice = 0;
        for (Concurso c : listConcurso) {
            if (c.getCodConcurso() == cod) {
                indice = listConcurso.indexOf(c);
            }
        }
        listConcurso.get(indice).getParticipantes().add(part);
    }

    /**
     * Método para asignar un ganador ya sea de manera aleatoria o manual
     *
     * @author Jeffrey Prado
     */
    public void asignarGanador() {
        System.out.println("Ingrese codigo de concurso en el que desea agregar ganador: ");
        int cod = ingreso.nextInt();

        int indice = 0;
        for (Concurso c : listConcurso) {
            if (c.getCodConcurso() == cod) {
                indice = listConcurso.indexOf(c);
            }
        }

        Concurso concEditar = listConcurso.get(indice);

        System.out.println("1.- Asignar ganador aleatoriamente\n"
                + "2.- Asignar ganador manualmente\n"
                + "Ingrese la opción que desea: ");
        String op = ingreso.next();

        if (op.equals("1")) {
            Random n = new Random();
            int partRand = n.nextInt(listParticipantes.size());
            Persona ganadorR = listParticipantes.get(partRand);
            System.out.println(ganadorR.getCedula() + " - " + ganadorR.getNombre() + " - " + ganadorR.getApellidos() + " - " + ganadorR.getTelefono());
            concEditar.setGanador(ganadorR);
        } else if (op.equals("2")) {
            System.out.println("Ingrese CI del participante: ");
            String cI = ingreso.next();
            int indiceG = 0;
            for (Persona g : listParticipantes) {
                if (g.getCedula().equals(cI)) {
                    indiceG = listParticipantes.indexOf(g);
                }
            }
            Persona ganadorM = listParticipantes.get(indiceG);
            System.out.println(ganadorM.getCedula() + " - " + ganadorM.getNombre() + " - " + ganadorM.getApellidos() + " - " + ganadorM.getTelefono());
            concEditar.setGanador(ganadorM);
        }
    }

    /**
     * Método que permite administrar los top 5 de la radio (opción 4 del menú
     * principal)
     *
     * @author Jeffrey Prado
     */
    public void admiTop5() {
        String subMenu = "\n"
                + "1.- Registrar Top 5\n"
                + "2.- Mostrar Top 5\n"
                + "Cualquier caracter para volver al menu principal\n"
                + "Ingrese la opción que desea: ";

        System.out.println(subMenu);

        String sub = ingreso.next();

        if (sub.equals("1")) {
            registrarTop5();
        } else if (sub.equals("2")) {
            mostrarTop5();
        }
    }

    /**
     * Método para registrar un top 5
     *
     * @author Jeffrey Prado
     */
    public void registrarTop5() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            System.out.println("Ingresar fecha de TOP 5: ");
            Date fecha = formatter.parse(ingreso.next());
            ArrayList<Cancion> listCanciones = new ArrayList<Cancion>();
            int cont = 1;
            while (cont < 6) {
                System.out.println("Ingrese cancion #" + cont);
                String cancion = ingreso.next();
                System.out.println("Ingrese el nombre del artista: ");
                String art = ingreso.next();
                listCanciones.add(new Cancion(art, cancion, cont));
            }
            Top top = new Top(fecha, listCanciones);
            listTop.add(top);

        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    /**
     * Método para mosrtar los top 5 registrados en orden descendente de acuerdo
     * a la fecha
     *
     * @author Alexis Poveda
     * @author Jeffrey Prado
     */
    public void mostrarTop5() {
        ArrayList<Date> fechas = new ArrayList<Date>();
        for (Top fe : listTop) {
            fechas.add(fe.getFecha());
        }
        Collections.sort(fechas);
        for (Date feOrd : fechas) {
            for (Top t : listTop) {
                if (feOrd == t.getFecha()) {
                    System.out.println("Fecha: " + t.getFecha());
                    for (int i = 0; i < t.getCancionesTopFecha().size(); i++) {
                        System.out.println("\t" + (i + 1) + ".- " + t.getCancionesTopFecha().get(i).getNomCancion());
                    }
                }
            }
            System.out.println("");
        }

    }

}
